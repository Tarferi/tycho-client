package net.ithief.tycho.data;

import java.util.ArrayList;
import java.util.List;

public class TychoUser {

	private String username;
	private String displayname;
	private STATUS status;

	private List<UserChangeListener> callbacks = new ArrayList<>();

	public TychoUser(String username, String displayname, STATUS status) {
		this.username = username;
		this.displayname = displayname;
		this.status = status;
	}

	public STATUS getStatus() {
		return status;
	}

	public void updateDisplayName(String newDisplayName) {
		synchronized (callbacks) {
			this.displayname = newDisplayName;
			for (UserChangeListener callback : callbacks) {
				callback.userChanged(this);
			}
		}
	}

	public void updateStatus(STATUS newStatus) {
		synchronized (callbacks) {
			this.status = newStatus;
			for (UserChangeListener callback : callbacks) {
				callback.userChanged(this);
			}
		}
	}

	public String getUsername() {
		return username;
	}

	public String getDisplayName() {
		return displayname;
	}

	public void addListener(UserChangeListener listener) {
		synchronized (callbacks) {
			if (!callbacks.contains(listener)) {
				callbacks.add(listener);
			}
		}
	}

	public void removeListener(UserChangeListener listener) {
		synchronized (callbacks) {
			if (callbacks.contains(listener)) {
				callbacks.remove(listener);
			}
		}
	}

}
