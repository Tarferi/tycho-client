package net.ithief.tycho.ui.components;

import javax.swing.JLabel;

public class MJLabel extends JLabel {

	private static final long serialVersionUID = 842109275813079745L;
	
	public MJLabel(String text) {
		super(text);
		setOpaque(false);
	}

}
