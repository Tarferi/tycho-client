package net.ithief.tycho.ui;

import net.ithief.tycho.MyUI;
import net.ithief.tycho.data.TychoUser;
import net.ithief.tycho.data.UserChangeListener;
import net.ithief.tycho.ui.components.MJPanel;
import net.miginfocom.swing.MigLayout;
import javax.swing.ImageIcon;
import javax.swing.JComponent;

import net.ithief.tycho.ui.components.MJLabel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

public class UserListItemUI extends MJPanel {

	private static final long serialVersionUID = 1700549284028325721L;
	private TychoUser user;
	private final JComponent lst;

	public TychoUser getUser() {
		return user;
	}

	private UserChangeListener changeListener = new UserChangeListener() {

		@Override
		public void userChanged(TychoUser user) {
			MyUI.invoke(new Runnable() {

				@Override
				public void run() {
					lst.invalidate();
					lst.repaint();
				}

			});

		}

	};
	private MJLabel lblUsername;
	private MJLabel lblIcon;

	@Override
	public void paintComponent(Graphics g) {
		if(this.hasFocus()) {
			this.setBackground(Color.red);
		} else {
			this.setBackground(Color.blue);
		}
	}

	public UserListItemUI(TychoUser user, JComponent lst) {
		setPreferredSize(new Dimension(240, 60));
		this.user = user;
		this.lst = lst;
		setLayout(new MigLayout("", "[][10][grow]", "[grow]"));

		lblIcon = new MJLabel("");
		lblIcon.setIcon(new ImageIcon(UserListItemUI.class.getResource("/net/ithief/tycho/icons/status_1_sm.png")));
		add(lblIcon, "cell 0 0");

		lblUsername = new MJLabel((String) null);
		lblUsername.setText("<Username>");
		add(lblUsername, "cell 2 0");
		user.addListener(changeListener);
		setOpaque(true);
	}

}
