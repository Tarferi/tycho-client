package net.ithief.tycho.ui;

import java.awt.Component;

import net.ithief.tycho.ui.components.MJPanel;
import javax.swing.BoxLayout;
import javax.swing.JComponent;

public class MyList extends MJPanel {

	private static final long serialVersionUID = 8729173158949931286L;

	public MyList() {
		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		setLayout(layout);
		this.setAlignmentY(TOP_ALIGNMENT);

	}

	public void addItem(JComponent c) {
		c.setAlignmentY(TOP_ALIGNMENT);
		this.add(c);
		this.setVisible(false);
		this.setVisible(true);
	}

	public void removeItem(Component c) {
		this.remove(c);
	}

}
