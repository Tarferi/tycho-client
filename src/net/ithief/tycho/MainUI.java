package net.ithief.tycho;

import java.lang.reflect.InvocationTargetException;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import net.ithief.tycho.ui.AppUI;
import net.ithief.tycho.ui.ChatUI;
import net.ithief.tycho.ui.LoginUI;

import java.awt.BorderLayout;

public class MainUI extends JFrame {

	private static final long serialVersionUID = 1L;
	private Connector conn = createConnector();

	private final LoginUI loginUI = new LoginUI(this);
	private final ChatUI chatUI = new ChatUI(this);

	private AppUI currentComponent;

	public Connector getConnector() {
		return conn;
	}

	private Connector createConnector() {
		return new Connector("127.0.0.1", 6005, new SupraMethod(this, "_connectionError", Object.class), chatUI);
	}

	public void _connectionError(Object data) {
		loginUI.setErrorText("Connection lost");
		conn = createConnector();
		setLoginUI();
	}

	public MainUI() {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {

				@Override
				public void run() {
					constructUI();
				}

			});
		} catch (InvocationTargetException | InterruptedException e) {
			e.printStackTrace();
		}
	}

	protected void constructUI() {
		setSize(640, 480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout(0, 0));
		setLoginUI();
		setVisible(true);
	}

	private void _setUI(AppUI panel) {
		if (currentComponent != null) {
			getContentPane().remove(currentComponent);
		}
		currentComponent = panel;
		getContentPane().add(currentComponent, BorderLayout.CENTER);
		currentComponent.reset();
		this.invalidate();
		this.repaint();
	}

	private void setUI(final AppUI panel) {

		if (SwingUtilities.isEventDispatchThread()) {
			_setUI(panel);
		} else {
			try {
				SwingUtilities.invokeAndWait(new Runnable() {

					@Override
					public void run() {
						_setUI(panel);
					}

				});
			} catch (InvocationTargetException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void setChatUI() {
		setUI(chatUI);
	}

	public void setLoginUI() {
		setUI(loginUI);
		loginUI.setDisabledUI(false);
	}

}
