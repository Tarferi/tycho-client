package net.ithief.tycho;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class SupraMethod {

	private Method method;
	private Object instance;

	public SupraMethod(Object instance, String methodName) {
		this.instance = instance;
		try {
			method = instance.getClass().getMethod(methodName, Object.class);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		if (method == null) {
			System.err.println("Callback method not found: " + methodName);
		}
	}

	public SupraMethod(Object instance, String methodName, Class<?>... objects) {
		this.instance = instance;
		try {
			method = instance.getClass().getMethod(methodName, objects);
		} catch (NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
		}
		if (method == null) {
			System.err.println("Callback method not found: " + methodName);
		}
	}

	public void invoke() {
		try {
			method.invoke(instance);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public void invoke(Object... objects) {
		try {
			method.invoke(instance, objects);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public void invokeNull() {
		try {
			method.invoke(instance, (Object) null);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
