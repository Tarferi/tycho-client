package net.ithief.tycho.ui.components;

import java.awt.AWTEvent;
import java.awt.AWTEventMulticaster;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

public class MJButton extends JButton {

	private static final long serialVersionUID = 3479976422897347205L;

	public MJButton(String text) {
		super(text);
		setOpaque(false);
		setRounded(40);
		this.setMinimumSize(new Dimension(120, 50));
	}

	ActionListener actionListener;
	protected boolean pressed = false;
	private int radius;

	private void setRounded(int radius) {
		this.radius = radius;
		enableEvents(AWTEvent.MOUSE_EVENT_MASK);
	}

	@Override
	public void paint(Graphics g) {
		if (pressed) {
			g.setColor(getBackground().darker().darker());
		} else {
			g.setColor(getBackground());
		}
		if (!super.isEnabled()) {
			g.setColor(getBackground().darker().darker());
		}
		g.fillRoundRect(0, 0, getWidth() - 1, getHeight() - 1, radius, radius);
		g.setColor(getBackground().darker().darker().darker());
		g.drawRoundRect(0, 0, getWidth() - 1, getHeight() - 1, radius, radius);
		Font f = getFont();
		if (f != null) {
			FontMetrics fm = getFontMetrics(getFont());
			g.setColor(isEnabled() ? getForeground() : getForeground().brighter().brighter());
			g.drawString(getText(), getWidth() / 2 - fm.stringWidth(getText()) / 2, getHeight() / 2 + fm.getMaxDescent());
		}
	}

	public void addActionListener(ActionListener listener) {
		actionListener = AWTEventMulticaster.add(actionListener, listener);
		enableEvents(AWTEvent.MOUSE_EVENT_MASK);
	}

	public void removeActionListener(ActionListener listener) {
		actionListener = AWTEventMulticaster.remove(actionListener, listener);
	}

	@Override
	public boolean contains(int x, int y) {
		int mx = getSize().width / 2;
		int my = getSize().height / 2;
		return (((mx - x) * (mx - x) + (my - y) * (my - y)) <= mx * mx);
	}

	@Override
	public void processMouseEvent(MouseEvent e) {
		switch (e.getID()) {
		case MouseEvent.MOUSE_PRESSED:
			pressed = true;
			repaint();
			break;
		case MouseEvent.MOUSE_RELEASED:
			if (actionListener != null) {
				actionListener.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, getText()));
			}
			if (pressed == true) {
				pressed = false;
				repaint();
			}
			break;
		case MouseEvent.MOUSE_ENTERED:

			break;
		case MouseEvent.MOUSE_EXITED:
			if (pressed == true) {
				pressed = false;
				repaint();
			}
			break;
		}
		super.processMouseEvent(e);
	}
}
