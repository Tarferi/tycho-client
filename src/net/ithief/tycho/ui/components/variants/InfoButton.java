package net.ithief.tycho.ui.components.variants;

import net.ithief.tycho.ui.components.MJButton;

import java.awt.Color;

public class InfoButton extends MJButton {

	private static final long serialVersionUID = -9052438727740487460L;

	public InfoButton(String text) {
		super(text);
		setBackground(new Color(30, 144, 255));
	}
}
