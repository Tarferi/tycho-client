package net.ithief.tycho.ui;

import net.ithief.tycho.MainUI;
import net.ithief.tycho.MyUI;
import net.ithief.tycho.SupraMethod;
import net.miginfocom.swing.MigLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import net.ithief.tycho.ui.components.MJPanel;
import net.ithief.tycho.ui.components.MJTextField;
import net.ithief.tycho.ui.components.MJLabel;
import net.ithief.tycho.ui.components.MJPasswordField;
import net.ithief.tycho.ui.components.variants.InfoButton;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import net.ithief.tycho.ui.components.variants.ErrorLabel;

public class LoginUI extends AppUI {
	private static final long serialVersionUID = 1L;
	private MJTextField txtUsername;
	private MJPasswordField txtPassword;
	private InfoButton btnLogin;
	private MainUI mui;

	private ErrorLabel lblError;

	public LoginUI(MainUI mui) {
		setOpaque(true);
		setBackground(new Color(240, 255, 240));
		this.mui = mui;
		setLayout(new MigLayout("", "[grow][450][grow]", "[grow][240][grow]"));

		MJPanel panel = new MJPanel();
		panel.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Login", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		add(panel, "cell 1 1,grow");
		panel.setLayout(new MigLayout("", "[][grow]", "[30][30][30][30][50][]"));

		MJLabel lblUsername = new MJLabel("Username:");
		lblUsername.setText("Username");
		panel.add(lblUsername, "cell 0 0 2 1,alignx center,growy");

		txtUsername = new MJTextField();
		txtUsername.setText("Encorn");
		panel.add(txtUsername, "cell 1 1,grow");
		txtUsername.setColumns(10);

		MJLabel lblPassword = new MJLabel("Password:");
		lblPassword.setText("Password");
		panel.add(lblPassword, "cell 1 2,alignx center,growy");

		txtPassword = new MJPasswordField();
		txtPassword.setText("Kae");
		panel.add(txtPassword, "cell 1 3,grow");
		txtPassword.setColumns(10);

		btnLogin = new InfoButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				handleLogin(txtUsername.getText(), txtPassword.getText());
			}
		});

		lblError = new ErrorLabel((String) null);
		lblError.setText("This is error");
		panel.add(lblError, "cell 1 4,alignx center");
		panel.add(btnLogin, "cell 1 5,grow");
		setError(null);
	}

	protected void _reset() {
		setErrorText(null);
	}

	public void setErrorText(final String errorText) {
		MyUI.invoke(new Runnable() {

			@Override
			public void run() {
				setError(errorText);
			}

		});
	}

	private void setError(String error) {
		if (error == null) {
			error = "";
		}
		final String myError = error;
		MyUI.invoke(new Runnable() {

			@Override
			public void run() {
				lblError.setText(myError);
			}

		});
	}

	private void handleLogin(String username, String password) {
		if (username.length() > 0 && username.length() < 30 && password.length() > 0 && password.length() < 30) {
			setDisabledUI(true);
			mui.getConnector().tryLogin(username, password, new SupraMethod(this, "_handleLoginCallback", Object.class));
		} else {
			setError("Invalid username or password");
		}
	}

	public void _handleLoginCallback(Object data) {
		boolean res = (boolean) data;
		if (res) { // Logged in
			mui.setChatUI();
		} else {
			setError("Login failed");
		}
		setDisabledUI(false);
	}

	public void setDisabledUI(final boolean disabled) {
		MyUI.invoke(new Runnable() {

			@Override
			public void run() {
				txtUsername.setEnabled(!disabled);
				txtPassword.setEnabled(!disabled);
				btnLogin.setEnabled(!disabled);
			}

		});

	}

}
