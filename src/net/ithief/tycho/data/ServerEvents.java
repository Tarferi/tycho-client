package net.ithief.tycho.data;

public interface ServerEvents {

	public void UserStatusChange(String username, STATUS newStatus);
	
	public void FriendshipRequestReceived(String username);
	
}
