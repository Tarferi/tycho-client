package net.ithief.tycho.ui;

import net.ithief.tycho.MyUI;
import net.ithief.tycho.ui.components.MJPanel;

public abstract class AppUI extends MJPanel {

	private static final long serialVersionUID = 1L;

	protected abstract void _reset();
	

	public void reset() {
		MyUI.invoke(new Runnable() {

			@Override
			public void run() {
				_reset();
				setVisible(false);
				setVisible(true);
				invalidate();
				repaint();
			}

		});
	}
	
}
