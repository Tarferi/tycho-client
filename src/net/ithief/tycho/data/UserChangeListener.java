package net.ithief.tycho.data;

public interface UserChangeListener {

	public void userChanged(TychoUser user);
	
}
