package net.ithief.tycho.ui.components;

import javax.swing.JPasswordField;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class MJPasswordField extends JPasswordField {
	public MJPasswordField() {
		setBorder(new CompoundBorder(new LineBorder(new Color(0, 0, 0)), new EmptyBorder(8, 8, 8, 8)));
		setOpaque(false);
	}

	private static final long serialVersionUID = 2678169243137384252L;

	@Override
	public String getText() {
		return new String(super.getPassword());
	}
}
