package net.ithief.tycho.ui.components;

import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;

public class MJTextField extends JTextField {
	public MJTextField() {
		setBorder(new CompoundBorder(new LineBorder(new Color(0, 0, 0)), new EmptyBorder(8, 8, 8, 8)));
		setOpaque(false);
	}

	private static final long serialVersionUID = 9105238492974752137L;

}
