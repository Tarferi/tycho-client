package net.ithief.tycho.ui.components.variants;

import java.awt.Color;

import net.ithief.tycho.ui.components.MJLabel;

public class ErrorLabel extends MJLabel {

	private static final long serialVersionUID = 6938307245810961675L;

	public ErrorLabel(String text) {
		super(text);
		this.setForeground(Color.red);
	}

}
