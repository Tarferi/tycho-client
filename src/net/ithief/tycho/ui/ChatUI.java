package net.ithief.tycho.ui;

import net.ithief.tycho.MainUI;
import net.ithief.tycho.MyUI;
import net.ithief.tycho.SupraMethod;
import net.ithief.tycho.data.STATUS;
import net.ithief.tycho.data.ServerEvents;
import net.ithief.tycho.data.TychoUser;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JTextPane;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.awt.event.ActionEvent;
import net.ithief.tycho.ui.components.variants.ErrorLabel;

public class ChatUI extends AppUI implements ServerEvents {
	private static final long serialVersionUID = 1L;
	private JTextField txtSearch;
	private JButton btnAdd;

	private MainUI mui;
	private ErrorLabel rlblNoSuchUser;

	private final JList<UserListItemUI> userLst;
	private final DefaultListModel<UserListItemUI> listModel = new DefaultListModel<>();

	private Map<String, UserListItemUI> userList = new HashMap<>();

	public void registerUser(TychoUser user) {
		synchronized (userList) {
			UserListItemUI ui = new UserListItemUI(user, userLst);
			userList.put(user.getUsername(), ui);
			listModel.addElement(ui);
		}
	}

	public void unregisterUser(String username) {
		synchronized (userList) {
			if (userList.containsKey(username)) {
				UserListItemUI item = userList.remove(username);
				listModel.removeElement(item);
			}
		}
	}

	public ChatUI(MainUI mainUI) {
		mui = mainUI;
		setLayout(new MigLayout("", "[][][grow]", "[][][grow]"));

		JLabel lblLoggedInAs = new JLabel("Logged in as:");
		add(lblLoggedInAs, "cell 0 0");

		JLabel lblUsername = new JLabel("<Username>");
		add(lblUsername, "cell 1 0,alignx left,aligny bottom");

		txtSearch = new JTextField();
		add(txtSearch, "cell 0 1,growx");
		txtSearch.setColumns(10);

		btnAdd = new JButton("Add a friend");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String addUsername = txtSearch.getText();
				addFriend(addUsername);
			}
		});
		add(btnAdd, "cell 1 1");

		rlblNoSuchUser = new ErrorLabel((String) null);
		rlblNoSuchUser.setText("");
		add(rlblNoSuchUser, "cell 2 1");

		userLst = new JList<>();
		userLst.setCellRenderer(new UserListItemUIRenderer());
		userLst.setModel(listModel);
		add(userLst, "cell 0 2 2 1,grow");

		JTextPane txtChat = new JTextPane();
		add(txtChat, "cell 2 2,grow");
		setVisible(true);
		registerUser(new TychoUser("Encorn", "Just Encorn", STATUS.ONLINE));
	}

	@Override
	protected void _reset() {
		setFriendshipError(null);
		txtSearch.setText("");
	}

	private void setFriendshipError(String error) {
		if (error == null) {
			error = "";
		}
		rlblNoSuchUser.setText(error);
	}

	public void friendshiprequestcallback(final Object data) {
		MyUI.invoke(new Runnable() {

			@Override
			public void run() {
				int code = (int) data;
				if (code == 0) { // Already a friend
					setFriendshipError("This user is already a friend");
				} else if (code == 1) {
					setFriendshipError("No such user exists");
				} else if (code == 2) {
					setFriendshipError("Request sent");
				}
				txtSearch.setEnabled(true);
				btnAdd.setEnabled(true);
			}

		});
	}

	protected void addFriend(String addUsername) {
		txtSearch.setEnabled(false);
		btnAdd.setEnabled(false);
		mui.getConnector().sendFriendshipRequest(addUsername, new SupraMethod(this, "friendshiprequestcallback", Object.class));
	}

	@Override
	public void UserStatusChange(String username, STATUS newStatus) {
		synchronized (userList) {
			if (!userList.containsKey(username)) { // New user
				registerUser(new TychoUser(username, "", newStatus));
			} else {
				userList.get(username).getUser().updateStatus(newStatus);
			}
		}
	}

	@Override
	public void FriendshipRequestReceived(String username) {
		// TODO Auto-generated method stub

	}

}
