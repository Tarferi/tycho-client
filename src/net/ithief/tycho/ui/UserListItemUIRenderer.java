package net.ithief.tycho.ui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import net.ithief.tycho.ui.components.MJPanel;
import net.miginfocom.swing.MigLayout;
import net.ithief.tycho.data.STATUS;
import net.ithief.tycho.data.TychoUser;
import net.ithief.tycho.ui.components.MJLabel;
import javax.swing.ImageIcon;

public class UserListItemUIRenderer extends MJPanel implements ListCellRenderer<UserListItemUI> {
	private MJLabel lblUsername;
	private MJLabel lblIcon;

	private static final ImageIcon[] icons;

	static {
		int totalIcons = STATUS.values().length;
		icons = new ImageIcon[totalIcons];
		for (int i = 0; i < totalIcons; i++) {
			icons[i] = new ImageIcon(UserListItemUIRenderer.class.getResource("/net/ithief/tycho/icons/status_" + i + "_sm.png"));
		}
	}

	private Color selectedColor = new Color(0xc9f0ff);
	private Color defaultColor = Color.black;

	private Color borderColor = new Color(0x23c2ff);
	private Border selectedBorder = new LineBorder(borderColor, 2);
	private Border defaultBorder = new EmptyBorder(2, 2, 2, 2);

	public UserListItemUIRenderer() {
		setLayout(new MigLayout("", "[][10][grow]", "[grow]"));

		lblIcon = new MJLabel((String) null);
		lblIcon.setIcon(icons[0]);
		lblIcon.setText("");
		add(lblIcon, "cell 0 0");

		lblUsername = new MJLabel((String) null);
		lblUsername.setText("<Username>");
		add(lblUsername, "cell 2 0");
	}

	private static final long serialVersionUID = 1L;

	@Override
	public Component getListCellRendererComponent(JList<? extends UserListItemUI> list, UserListItemUI value, int index, boolean isSelected, boolean cellHasFocus) {
		TychoUser user = value.getUser();
		lblIcon.setIcon(icons[user.getStatus().getCode()]);
		lblUsername.setText(user.getUsername());
		this.setOpaque(isSelected);
		this.setBackground(isSelected ? selectedColor : defaultColor);
		this.setBorder(isSelected ? selectedBorder : defaultBorder);
		return this;
	}

}
