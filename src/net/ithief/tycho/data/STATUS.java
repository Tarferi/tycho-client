package net.ithief.tycho.data;

public enum STATUS {

	ONLINE(0), OFFLINE(1), FRIEND_PENDING(2);

	private int code;

	public int getCode() {
		return code;
	}

	private STATUS(int code) {
		this.code = code;
	}

}
