package net.ithief.tycho;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

public class MyUI {

	public static void invoke(Runnable r) {
		if (SwingUtilities.isEventDispatchThread()) {
			r.run();
		} else {
			try {
				SwingUtilities.invokeAndWait(r);
			} catch (InvocationTargetException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
}
