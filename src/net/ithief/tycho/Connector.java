package net.ithief.tycho;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.ithief.tycho.data.ServerEvents;

public class Connector {

	private String ip;
	private int port;
	private SupraMethod errorCallback;

	private final ServerEvents events;

	public Connector(String ip, int port, SupraMethod errorCallback, ServerEvents eventCallback) {
		this.ip = ip;
		this.port = port;
		this.errorCallback = errorCallback;
		events = eventCallback;
	}

	private List<ConnectorRequest> requests = new ArrayList<>();

	private Map<Integer, ConnectorRequest> callbacks = new HashMap<>();

	private Socket sock;
	private InputStream in;
	private OutputStream out;
	private boolean run = true;

	private boolean waitingForRequest = false;

	private void close() {
		run = false;
		try {
			sock.close();
		} catch (Throwable e) {
		}
		try {
			in.close();
		} catch (Throwable e) {
		}
		try {
			out.close();
		} catch (Throwable e) {
		}
		errorCallback.invokeNull();
	}

	private boolean threadRunning = false;

	private Thread _thread = new Thread() {
		@Override
		public void run() {
			setName("Socket reader");
			threadRunning = true;
			try {
				sock = new Socket(ip, port);
				in = sock.getInputStream();
				out = sock.getOutputStream();
			} catch (IOException e) {
				e.printStackTrace();
				close();
			}
			_thread2.start();
			requestSatisfier();
		}
	};

	private Thread _thread2 = new Thread() {
		@Override
		public void run() {
			setName("Socket writer");
			while (run) {
				try {
					int messageID = readInt();
					System.out.println("Received message ID: " + messageID);
					if (messageID != 0) { // This is a response to something
						ConnectorRequest callback;
						synchronized (callbacks) {
							callback = callbacks.remove(messageID);
						}
						if (callback == null) { // The fuck?
							close();
							throw new IOException();
						}
						callback.handleResponse();
					} else {
						handleUnknownPacket();
					}
				} catch (IOException e) {
					e.printStackTrace();
					close();
				}
			}
		}
	};

	private void requestSatisfier() {
		while (run) {
			ConnectorRequest request;
			synchronized (requests) {
				if (requests.isEmpty()) {
					try {
						waitingForRequest = true;
						requests.wait();
						waitingForRequest = false;
					} catch (InterruptedException e) {
						e.printStackTrace();
						close();
						return;
					}
				}
				request = requests.remove(0);
			}
			handleRequest(request);
		}
	}

	protected void handleUnknownPacket() {
		synchronized (this) {
			try {
				MESSAGES msg = MESSAGES.get(readInt());
				switch (msg) {
				case USER_STATUS_CHANGED:
					int StatusCode = readInt();
					String username = readString();
					break;
				default:
					System.out.println("Unhandled packet: " + msg.toString() + " (" + msg.getCode() + ")");
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
				close();
			}
		}
	}

	private void handleRequest(ConnectorRequest request) {
		request.superHandle();
	}

	private int requestIDCounter = 1000;

	private abstract class ConnectorRequest {
		private final Object[] data;
		private final SupraMethod callback;
		private final int requestID;

		protected String getString(int index) {
			return (String) data[index];
		}

		private void superHandle() {
			synchronized (this) {
				handle();
			}
		}

		public void writeID() {
			try {
				writeInt(requestID);
				synchronized (callbacks) {
					callbacks.put(requestID, this);
				}
			} catch (IOException e) {
				e.printStackTrace();
				close();
			}

		}

		protected abstract void handle();

		protected abstract void handleResponse();

		private ConnectorRequest(SupraMethod callback, Object... objects) {
			synchronized (this) {
				this.data = objects;
				this.callback = callback;
				this.requestID = requestIDCounter;
				requestIDCounter++;
			}
		}

		public void invokeCallback(Object data) {
			callback.invoke(data);
		}
	}

	private class FriendshipRequest extends ConnectorRequest {
		FriendshipRequest(String username, SupraMethod callback) {
			super(callback, username);
		}

		@Override
		protected void handle() {
			try {
				writeInt(MESSAGES.SEND_FRIEND_REQUEST.getCode());
				writeID();
				writeString(getString(0));
			} catch (IOException e) {
				e.printStackTrace();
				close();
			}
		}

		@Override
		protected void handleResponse() {
			try {
				int code = readInt();
				super.invokeCallback(code);
			} catch (IOException e) {
				e.printStackTrace();
				close();
			}
		}

	}

	private class LoginRequest extends ConnectorRequest {
		LoginRequest(String username, String password, SupraMethod callback) {
			super(callback, username, password);
		}

		@Override
		protected void handle() {
			try {
				writeID();
				writeString(getString(0)); /* Username */
				writeString(getString(1)); /* Password */
			} catch (IOException e) {
				e.printStackTrace();
				close();
			}
		}

		@Override
		protected void handleResponse() {
			try {
				int code = readInt();
				if (code == 0) {
					super.invokeCallback(true);
				}
			} catch (IOException e) {
				close();
				e.printStackTrace();
			}
			super.invokeCallback(false);
		}
	}

	private void addRequest(ConnectorRequest request) {
		synchronized (requests) {
			requests.add(request);
			if (waitingForRequest) {
				requests.notify();
			}
			if (!threadRunning) {
				_thread.start();
			}
		}
	}

	public void tryLogin(String username, String password, SupraMethod callback) {
		addRequest(new LoginRequest(username, password, callback));
	}

	public void sendFriendshipRequest(String username, SupraMethod callback) {
		addRequest(new FriendshipRequest(username, callback));
	}

	private int readByte() throws IOException {
		int i = in.read();
		if (i >= 0) {
			return i;
		} else {
			throw new IOException("Socket closed?");
		}
	}

	private int readShort() throws IOException {
		return (readByte() << 8) | readByte();
	}

	protected int readInt() throws IOException {
		return (readShort() << 16) | readShort();
	}

	private byte[] readArray(int length) throws IOException {
		byte[] buffer = new byte[length];
		in.read(buffer, 0, length);
		return buffer;
	}

	protected String readString() throws IOException {
		return new String(readArray(readInt()));
	}

	private void writeByte(int data) throws IOException {
		out.write(data & 0xff);
	}

	private void writeShort(int data) throws IOException {
		writeByte(data >> 8);
		writeByte(data);
	}

	protected void writeInt(int data) throws IOException {
		writeShort(data >> 16);
		writeShort(data);
	}

	private void writeArray(byte[] data) throws IOException {
		out.write(data, 0, data.length);
	}

	protected void writeString(String str) throws IOException {
		writeInt(str.length());
		writeArray(str.getBytes());
	}

	enum MESSAGES {
		SEND_MESSAGE(0), SEND_GROUP_MESSAGE(1), SEND_FRIEND_REQUEST(2), RECEIVED_FRIEND_REQUEST(3), ACCEPT_FRIEND_REQUEST(4), REJECT_FRIEND_REQUEST(5), CREATE_GROUP(6), DISPOSE_GROUP(7), GET_GROUP(8), UNKNOWN(9), USER_STATUS_CHANGED(10);

		private final int opCode;
		private static final int size = MESSAGES.values().length;

		MESSAGES(int opCode) {
			this.opCode = opCode;
		}

		public int getCode() {
			return opCode;
		}

		public static MESSAGES get(int code) {
			if (code >= 0 && code < size) {
				return MESSAGES.values()[code];
			} else {
				return UNKNOWN;
			}
		}
	}
}
